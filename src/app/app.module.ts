import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { DemoFormSkuComponent } from './demo-form-sku/demo-form-sku.component';
import { FormWithValidationsComponent } from './form-with-validations/form-with-validations.component';
import { DemoForNgModelComponent } from './demo-for-ng-model/demo-for-ng-model.component';

@NgModule({
  declarations: [
    AppComponent,
    DemoFormSkuComponent,
    FormWithValidationsComponent,
    DemoForNgModelComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
