import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms/';

@Component({
  selector: 'app-demo-for-ng-model',
  templateUrl: './demo-for-ng-model.component.html',
  styleUrls: ['./demo-for-ng-model.component.css']
})
export class DemoForNgModelComponent implements OnInit {
  myForm: FormGroup;
  productName: string;

  constructor(fb: FormBuilder) {
    this.myForm = fb.group({
      "productName": ["", Validators.required]
    });

  }
  onSubmit(value: string): void {
    console.log("You submitted value", value);
  }
  ngOnInit() {
  }

}
