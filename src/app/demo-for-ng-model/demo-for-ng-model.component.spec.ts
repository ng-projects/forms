import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoForNgModelComponent } from './demo-for-ng-model.component';

describe('DemoForNgModelComponent', () => {
  let component: DemoForNgModelComponent;
  let fixture: ComponentFixture<DemoForNgModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoForNgModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoForNgModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
