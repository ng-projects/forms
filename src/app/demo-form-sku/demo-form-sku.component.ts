import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms"

@Component({
  selector: 'app-demo-form-sku',
  templateUrl: './demo-form-sku.component.html',
  styleUrls: ['./demo-form-sku.component.css']
})
export class DemoFormSkuComponent implements OnInit {
  myForm: FormGroup;

  constructor(fb: FormBuilder) {
    this.myForm = fb.group({
      "sku": ["ABC123"]
    });
  }

  ngOnInit() {
  }

  onSubmit (form: any): void {
    console.log("You submitted values", form);
  }

  myFormSubmit (form: any): void {
    console.log("You submitted values", form);
  }


}
