import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, AbstractControl} from "@angular/forms";
import { FormControl } from '@angular/forms/src/model';

@Component({
  selector: 'app-form-with-validations',
  templateUrl: './form-with-validations.component.html',
  styleUrls: ['./form-with-validations.component.css']
})
export class FormWithValidationsComponent implements OnInit {
  myForm: FormGroup;
  sku: AbstractControl;

  constructor(fb: FormBuilder) { 
    this.myForm = fb.group({
      "sku": ['', Validators.required],
      "custom" : ['', Validators.compose([Validators.required, this.customValidator])]
    });

    this.sku = this.myForm.controls["sku"];

    this.sku.valueChanges.subscribe((value: string) => {
      console.log("sku changed to:", value);
    })

    this.myForm.valueChanges.subscribe((form: any) => {
      console.log("form changed to: ", form);
    });
  }

  customValidator(control: FormControl): {[s: string]: boolean}{
    if (!control.value.match(/^123/)){
      return {invalidField: true}
    }
  }

  ngOnInit() {
  }

  onSubmit(value: string): void {
    console.log("You submitted value:", value);
  }

}
